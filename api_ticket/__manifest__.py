{
    "name": "API Ticket",
    "version": "14.0.1",
    "category": "Helpdesk",
    "summary": """
        REST API Odoo Helpdesk
    """,
    "author": "Aziz Adi Nugroho",
    "website": "https://medium.com/@azizadingrh",
    "depends": ["helpdesk_mgmt","base_rest_auth_api_key","base_rest_datamodel"],
    "data": [],
    "installable": True,
}
